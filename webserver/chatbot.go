package webserver

import (
	"bytes"
	"encoding/json"
	"errors"
	"gitlab.com/gleno/astar"
	"log"
	"net"
	"net/http"
	"strconv"
)

// ChatBot allows to send messages to a webhook
type ChatBot struct {
	config     *astar.Config
	webHookURL string
}

// NewChatBot creates a ChatBot that will send messages to webHookURL
func NewChatBot(config *astar.Config, webHookURL string) *ChatBot {
	return &ChatBot{config, webHookURL}
}

// PostStartUpMessage posts information like the local IP address
func (c *ChatBot) PostStartUpMessage(port int) error {

	uiURL, err := c.getUIURL(port)
	if err != nil {
		log.Println(err)
		return err
	}

	previouselypostedURLKey := c.config.Section("slack").Key("previouselypostedURL")
	if previouselypostedURLKey.String() == uiURL {
		log.Println("UI URL already posted, not posting again")
		return nil
	}

	welcomeMessage := "Hi, please reach me at " + uiURL

	bytesRepresentation, err := json.Marshal(map[string]interface{}{"text": welcomeMessage})
	if err != nil {
		return err
	}
	resp, err := http.Post(c.webHookURL, "application/json", bytes.NewBuffer(bytesRepresentation))
	if err != nil {
		return err
	}
	if resp.StatusCode != http.StatusOK {
		return errors.New("Resonse from Slack: " + resp.Status)
	}
	defer resp.Body.Close()
	previouselypostedURLKey.SetValue(uiURL)
	c.config.Save()

	return nil
}

// getOutboundIP returns the outbound IP of this machine
func (c *ChatBot) getOutboundIP() (string, error) {
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		return "", err
	}
	defer conn.Close()

	localAddr := conn.LocalAddr().(*net.UDPAddr)

	return localAddr.IP.String(), nil
}

// getUIURL returns the URL where the UI of the robot can be accessed
func (c *ChatBot) getUIURL(port int) (string, error) {
	ip, err := c.getOutboundIP()
	if err != nil {
		log.Println(err)
		log.Println("Cannot find any outbound IP address")
		return "", nil
	}
	return "http://" + ip + ":" + strconv.Itoa(port), nil
}
