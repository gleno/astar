package webserver

import (
	"context"
	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
	"github.com/gobuffalo/packr"
	"github.com/gorilla/websocket"
	"gitlab.com/gleno/astar/commands"
	"gitlab.com/gleno/astar/drivers"
	"gobot.io/x/gobot/platforms/raspi"
	"log"
	"math"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"time"
)

var version = "undefined"

type serverData struct {
	robot  *commands.Robot
	wsPort int
}

// GetAstar returns either a FakeAStar if the host does not have an Astar or a real AStarDriver
func GetAstar() drivers.AStar {
	var astar drivers.AStar
	r := raspi.NewAdaptor()
	err := r.Connect()
	if err != nil {
		log.Print("Cannot connect to raspberry ", err.Error())
	} else {
		astar = drivers.NewAStarDriver(r)
		err = astar.(*drivers.AStarDriver).Start()
		if err != nil {
			log.Print("Cannot connect to Astar ", err.Error())
			astar = nil
		}
	}

	if astar == nil {
		log.Print("WARNING: Using Fake Astar")
		astar = drivers.NewFakeAStar()
	}
	return astar
}

// GetRouter returns a *gin.Engine which serves both RestAPI and CLient side UI
func GetRouter(a drivers.AStar, port int) *gin.Engine {
	router := gin.Default()
	s := &serverData{commands.NewRobot(a), port}

	box := packr.NewBox("./client/build/")
	fs := localFile(&box)
	router.Use(static.Serve("/", fs))

	api := router.Group("api")
	api.PUT("/leds/red/:red/yellow/:yellow/green/:green", s.leds)
	api.PUT("/motors/left/:left/right/:right", s.motors)
	api.PUT("/rotate/angle/:angle/speed/:speed", s.rotate)
	api.PUT("/move/distance/:distance/speed/:speed", s.move)
	api.POST("/reset_odometry", s.resetOdometry)
	api.GET("/status", s.status)
	api.GET("/camera", s.camera)
	api.GET("/version", s.version)
	api.GET("/wsport", s.getWsPort)
	router.GET("/ws/status", func(c *gin.Context) {
		s.wsStatus(c.Writer, c.Request)
	})

	return router
}

// ListenAndServe creates a server which provides the UI client on the given port number
func ListenAndServe(port int) {
	srv := &http.Server{
		Addr:    ":" + strconv.Itoa(port),
		Handler: GetRouter(GetAstar(), port),
	}

	go func() {
		// service connections
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 5 seconds.
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutting down Server...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Error while shutting down server:", err)
	}
	log.Println("Server exited")
}

func (s *serverData) camera(c *gin.Context) {
	c.Redirect(307, "/assets/images/1.jpg")
}

func (s *serverData) version(c *gin.Context) {
	c.Data(http.StatusOK, "application/text; charset=utf-8", []byte(version))
}

func (s *serverData) leds(c *gin.Context) {
	red, err1 := strconv.ParseBool(c.Param("red"))
	yellow, err2 := strconv.ParseBool(c.Param("yellow"))
	green, err3 := strconv.ParseBool(c.Param("green"))
	if err1 != nil || err2 != nil || err3 != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Missing or invalid arguments"})
	} else {
		s.robot.Leds(red, yellow, green)
		c.JSON(http.StatusOK, gin.H{})
	}
}

func (s *serverData) motors(c *gin.Context) {
	left, err1 := strconv.ParseFloat(c.Param("left"), 64)
	right, err2 := strconv.ParseFloat(c.Param("right"), 64)
	if err1 != nil || err2 != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Missing or invalid arguments"})
	} else if math.Abs(left) > 1 || math.Abs(right) > 1 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Arguments must be between -1 and 1"})
	} else {
		s.robot.Speed(left, right)
		c.JSON(http.StatusOK, gin.H{})
	}
}

func (s *serverData) rotate(c *gin.Context) {
	angle, err1 := strconv.ParseFloat(c.Param("angle"), 64)
	speed, err2 := strconv.ParseFloat(c.Param("speed"), 64)
	if err1 != nil || err2 != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Missing or invalid arguments"})
	} else if math.Abs(speed) > 1 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Speed must be between -1 and 1"})
	} else {
		s.robot.Rotate(angle, speed)
		c.JSON(http.StatusOK, gin.H{})
	}
}

func (s *serverData) move(c *gin.Context) {
	distance, err1 := strconv.ParseFloat(c.Param("distance"), 64)
	speed, err2 := strconv.ParseFloat(c.Param("speed"), 64)
	if err1 != nil || err2 != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Missing or invalid arguments"})
	} else if math.Abs(speed) > 1 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Speed must be between -1 and 1"})
	} else {
		s.robot.Move(distance, speed)
		c.JSON(http.StatusOK, gin.H{})
	}
}

var wsupgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func (s *serverData) wsStatus(w http.ResponseWriter, r *http.Request) {
	conn, err := wsupgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Printf("Failed to set websocket upgrade: %+v\n", err)
		return
	}
	defer conn.Close()
	// TODO do a graceful close when the server is shutdown
	for {
		_, msg, err := conn.ReadMessage()
		if err != nil {
			log.Print("Error while reading from websocket: ", err)
			break
		}
		if string(msg) == "stop" {
			break
		}

		err = conn.WriteJSON(s.getStatus())
		if err != nil {
			log.Print("Error while writing to websocket: ", err)
			break
		}
	}
}

func (s *serverData) getWsPort(c *gin.Context) {
	c.Data(http.StatusOK, "application/text; charset=utf-8", []byte(strconv.Itoa(s.wsPort)))
}

// RobotStatus represents the robot's status
// TODO move to a place which can be shared with client code
type RobotStatus struct {
	Battery   float64
	Connected bool
	Position  [2]float64
	Speed     float64
	Distance  float64
	Buttons   [3]bool
	Yaw       float64
}

func (s *serverData) getStatus() RobotStatus {
	situation := s.robot.ReadSituation()
	one, two, three := s.robot.ReadButtons()
	return RobotStatus{
		s.robot.ReadBattery(),
		true,
		[2]float64{situation.X, situation.Y},
		situation.Velocity,
		situation.Distance,
		[3]bool{one, two, three},
		situation.Yaw,
	}
}

func (s *serverData) status(c *gin.Context) {
	c.JSON(http.StatusOK, s.getStatus())
}

func (s *serverData) resetOdometry(c *gin.Context) {
	s.robot.ResetOdometry()
	c.JSON(http.StatusOK, gin.H{})
}

type localFileSystem struct {
	*packr.Box
}

func localFile(box *packr.Box) *localFileSystem {
	return &localFileSystem{Box: box}
}

func (l *localFileSystem) Exists(prefix string, filepath string) bool {
	return l.Has(filepath)
}
