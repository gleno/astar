package webserver

import (
	"gitlab.com/gleno/astar"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestPostWelcomeMessage(t *testing.T) {
	called := false
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		if called {
			t.Fatal("Server is expected to have been called only once")
		}
		called = true
	}))
	// Close the server when test finishes
	defer server.Close()

	tmpfile, err := ioutil.TempFile("", "robot_test.ini")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(tmpfile.Name())

	c := NewChatBot(astar.NewConfig(tmpfile.Name()), server.URL)
	if c.PostStartUpMessage(1111) != nil {
		t.Fatal("Error was returned by server")
	}

	// Check that if we call a second time, no call is made to the server
	c.PostStartUpMessage(1111)
}

func TestPostWelcomeMessageNoWebHook(t *testing.T) {
	tmpfile, err := ioutil.TempFile("", "robot_test.ini")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(tmpfile.Name())

	c := NewChatBot(astar.NewConfig(tmpfile.Name()), "")
	if c.PostStartUpMessage(1111) == nil {
		t.Fatal("Error was expected to be returned")
	}
}
