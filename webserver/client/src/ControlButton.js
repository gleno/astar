import React, { Component } from 'react';
import JqxButton from './assets/jqwidgets-react/react_jqxbuttons';

class ControlButton extends Component {

    componentDidMount() {
        this.button.on('click', (event) => { this.props.onClick() });
    }
    render() {
        return <JqxButton width={50} height={50} imgWidth={40} imgHeight={40}
            imgSrc={"assets/images/" + this.props.imgSrc} theme='dark'
            ref={ref => this.button = ref} />
    }
    setOptions(options) {
        this.button.setOptions(options)
    }
}
export default ControlButton;