export function resetOdometry() {
    return fetch("api/reset_odometry", {
        method: 'POST'
    });
}

export function turnLeft() {
    return fetch("api/rotate/angle/-1.5707/speed/0.5", {
        method: 'PUT'
    });
}

export function turnRight() {
    return fetch("api/rotate/angle/1.5707/speed/0.5", {
        method: 'PUT'
    });
}

export function moveForward() {
    return fetch("api/move/distance/0.25/speed/0.5", {
        method: 'PUT'
    });
}

export function moveBackward() {
    return fetch("api/move/distance/-0.25/speed/0.5", {
        method: 'PUT'
    });
}

export function playNote() {
    var data = {
        notes: 'l16ceg>c'
    };
    return fetch("api/notes", {
        method: 'PUT',
        data: JSON.stringify(data),
        datamethod: 'json',
        contentmethod: 'application/json; charset=utf-8'
    });
}

export function stopRobot() {
    return setMotors(0, 0)
}

export function setMotors(left, right) {
    return fetch("/api/motors/left/" + left + "/right/" + right, {
        method: 'PUT',
    });
}

export function setLeds(red, yellow, green) {
    return fetch("api/leds/red/" + red + "/yellow/" + yellow + "/green/" + green, {
        method: 'PUT',
    });
}

export function getStatus() {
    return fetch("api/status", {
        method: 'GET',
    });
}

export function getWsPort() {
    return fetch("api/wsport", {
        method: 'GET',
    });
}

export function getVersion() {
    return fetch("api/version", {
        method: 'GET',
    });
}

/*
function shutdown() {
    if (confirm("Really shut down the Raspberry Pi?"))
        $.ajax({
            url: "api/shutdown",
            method: 'GET',
        });
}
*/