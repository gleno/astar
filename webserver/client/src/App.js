import React, { Component } from 'react';

import JqxTabs from './assets/jqwidgets-react/react_jqxtabs.js';
import Game from './Game'
import Drive from './Drive'
import Misc from './Misc'
import PageVisibility from 'react-page-visibility';

import "./assets/jqwidgets/styles/jqx.base.css"
import "./assets/jqwidgets/styles/jqx.dark.css"

class Tabs extends Component {
    componentDidMount() {
        this.refs.myTabs.on('selected', (event) => {
            this.setVisible(event.args.item === 0)
        });
    }

    setVisible(isVisible) {
        this.refs.Drive.setVisible(isVisible)
    }

    render() {
        return (
            <JqxTabs ref='myTabs'
                width={'100%'}
                height={'100%'}
                position={'top'}
                selectionTracker={false}
                scrollable={false}
                animationType={'fade'}
                theme='dark'
            >
                <ul>
                    <li>Drive</li>
                    <li>Jeu</li>
                    <li>Misc</li>
                </ul>
                <div><Drive ref="Drive" /></div>
                <div><Game /></div>
                <div><Misc /></div>
            </JqxTabs>
        )
    }
}

class App extends React.Component {
    handleVisibilityChange = isVisible => {
        this.refs.tabs.setVisible(isVisible);
    }

    render() {
        return (
            <PageVisibility onChange={this.handleVisibilityChange}>
                <Tabs ref="tabs" />
            </PageVisibility>
        );
    }
}

export default App;