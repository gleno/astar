import React, { Component } from 'react';
import JqxGauge from './assets/jqwidgets-react/react_jqxgauge';

export class BatteryGauge extends Component {
    render() {
        return <JqxGauge ref="gauge"
            ranges={[{
                startValue: 0, endValue: 6.5, style: { fill: '#FC6A6A', stroke: '#FC6A6A' }, endWidth: 1,
                startWidth: 1
            },
            {
                startValue: 6.5, endValue: 7, style: { fill: '#FCF06A', stroke: '#FCF06A' }, endWidth: 1,
                startWidth: 1
            },
            {
                startValue: 7, endValue: 10, style: { fill: '#0C9A9A', stroke: '#0C9A9A' }, endWidth: 1,
                startWidth: 1
            }]}
            ticksMajor={{ interval: 1, size: '9%' }}
            value={0}
            min={0}
            max={12}
            height={60}
            width={60}
            animationDuration={1}
            labels={{ visible: false }} />
    }

    setValue(value) {
        value = parseFloat(value.toFixed(2))
        if (this.value !== value) {
            this.value = value
            this.refs.gauge.value(value)
            this.refs.gauge.setOptions({
                caption: { value: value, position: 'bottom', offset: [0, 0], visible: true },
                value: value,
            })
        }
    }
}

export class SpeedGauge extends Component {
    render() {
        return <JqxGauge ref="gauge"
            ticksMinor={{ interval: 0.1, size: '5%' }}
            ticksMajor={{ interval: 0.2, size: '9%' }}
            value={0}
            min={0}
            max={2}
            height={60}
            width={60}
            animationDuration={100}
            labels={{ visible: false }}
        />
    }
    setValue(value) {
        value = parseFloat(value.toFixed(2))
        if (this.value !== value) {
            this.value = value
            this.refs.gauge.value(value)
            this.refs.gauge.setOptions({
                caption: { value: value, position: 'bottom', offset: [0, 0], visible: true },
                value: value,
            })
        }
    }
}
