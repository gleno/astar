import React, { Component } from 'react';
import JqxTextArea from './assets/jqwidgets-react/react_jqxtextarea';
import JqxButton from './assets/jqwidgets-react/react_jqxbuttons';
import JqxToggleButton from './assets/jqwidgets-react/react_jqxtogglebutton';
import { Col, Grid, Row, Image } from 'react-bootstrap';
import { resetOdometry, getWsPort } from './robot_api'
import { SpeedGauge, BatteryGauge } from './Gauge'
import Joystick from './Joystick'

class Drive extends Component {
    constructor(props) {
        super(props)
        this.statusPollTime = 100
        this.isVisible = true
        this.pollingEnabled = true
        this.robotStatus = {
            Connected: false,
        }
        this.statusWS = null
    }
    componentWillUnmount() {
        if (this.statusWS !== null && this.statusWS.readyState === WebSocket.OPEN) {
            this.statusWS.send("stop")
            this.statusWS.close()
        }
    }

    componentDidMount() {
        getWsPort().then(response => response.text())
            .then(port => {
                var ws_url = "ws://" + window.location.hostname + ":" + port + "/ws/status"
                var c = new WebSocket(ws_url);
                this.statusWS = c

                c.onmessage = function (event) {
                    this.updateStatus(JSON.parse(event.data))
                }.bind(this)

                c.onopen = function (event) {
                    this.pollStatus()
                }.bind(this)
                c.onerror = function (event) {
                    console.log("WebSocket error observed:", event);
                    this.robotStatus.Connected = false
                }.bind(this)
            })
            .catch(error => console.error(error));


        this.refs.resetOdometry.on('click', (_) => {
            resetOdometry()
            this.pollStatus()
        });
        this.refs.refreshToggle.on('click', (_) => {
            this.pollingEnabled = !this.pollingEnabled
            this.pollStatus()
            this.updateCameraRefresh()
        });
        this.pollStatus()
        this.updateCameraRefresh()
    }

    updateCameraRefresh() {
        this.refs.camera.setEnableRefresh(this.isVisible && this.pollingEnabled && this.robotStatus.Connected)
    }

    pollStatus() {
        if (this.statusWS && this.statusWS.readyState === WebSocket.OPEN) {
            this.statusWS.send("s")
        } else {
            this.robotStatus.Connected = false
        }
    }

    updateStatus(data) {
        if (!data.Connected) {
            this.robotStatus.Connected = false
        } else {
            this.robotStatus = data
            this.refs.batteryGauge.setValue(data.Battery)
            this.refs.speedGauge.setValue(data.Speed)
            this.refs.positionX.setValue(data.Position[0])
            this.refs.positionY.setValue(data.Position[1])

            if (this.pollingEnabled && this.isVisible) {
                setTimeout(this.pollStatus.bind(this), this.statusPollTime);
            }
        }
        this.updateCameraRefresh()
    }

    setVisible(isVisible) {
        this.isVisible = isVisible
        if (this.isVisible) {
            this.pollStatus()
        }
        this.updateCameraRefresh()
    }

    handleMouseUp(event) {
        this.refs.joystick.handleMouseUp(event)
    }

    handleMouseMove(event) {
        this.refs.joystick.handleMouseMove(event)
    }

    handleTouchMove(event) {
        this.refs.joystick.handleTouchMove(event)
    }

    render() {
        return (
            <Grid
                onMouseUp={this.handleMouseUp.bind(this)}
                onMouseMove={this.handleMouseMove.bind(this)}
            >
                <Row >
                    <Col xs={3} md={1}>
                        <TextStatus ref="positionX" />
                    </Col>
                    <Col xs={3} md={1}>
                        <TextStatus ref="positionY" />
                    </Col>
                    <Col xs={3} md={2}>
                        <JqxButton value={"Reset Odom"} ref='resetOdometry' theme={'dark'} />
                    </Col>
                    <Col xs={3} md={2}>
                        <JqxToggleButton value={"Refresh On"} ref='refreshToggle' toggled={this.pollingEnabled}
                            theme="dark" />
                    </Col>
                </Row>
                <Row className="show-grid">
                    <Col xs={2} md={1}>
                        <BatteryGauge ref="batteryGauge" />
                    </Col>
                    <Col xs={2} md={1}>
                        <SpeedGauge ref="speedGauge" />
                    </Col>
                </Row>
                <Row className="show-grid">
                    <Col xs={3} md={2}>
                        <Joystick ref='joystick' />
                    </Col>
                </Row>
                <Row className="show-grid">
                    <Col xs={9} md={8}>
                        <Camera ref="camera" className="img-responsive" responsive rounded enableRefresh={this.pollingEnabled} />
                    </Col>
                </Row>
            </Grid>)
    }
}

class Camera extends Component {
    constructor(props) {
        super(props)
        this.state = { cameraRef: "/api/camera?123" }
        this.enableRefresh = props.enableRefresh
        this.cameraPollTime = 100
        this.interval = null
    }
    render() {
        return <Image src={this.state.cameraRef} className="img-responsive" responsive rounded />
    }

    componentDidMount() {
        this.setEnableRefresh(this.enableRefresh)
    }

    setEnableRefresh(enableRefresh) {
        this.enableRefresh = enableRefresh
        if (enableRefresh) {
            if (this.interval === null) {
                this.interval = setInterval(this._update.bind(this), this.cameraPollTime)
            }
        } else if (this.interval !== null) {
            clearInterval(this.interval)
            this.interval = null
        }
    }

    _update() {
        if (this.enableRefresh) {
            this.setState({ cameraRef: "/api/camera?" + new Date().getTime() })
        }
    }
}


class TextStatus extends Component {
    constructor(props) {
        super(props)
        this.state = { value: 0 }
    }
    render() {
        return <JqxTextArea ref="status" theme='dark' value={this.state.value} />
    }

    setValue(value) {
        value = parseFloat(value.toFixed(3))
        if (this.value !== value) {
            this.value = value
            this.setState({ value: value })
        }
    }
}

export default Drive;