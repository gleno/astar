import React, { Component } from 'react';
import { Col, Grid, Row } from 'react-bootstrap';
import ControlButton from './ControlButton';
import Leds from './Leds'
import { moveForward, turnLeft, playNote, turnRight, moveBackward, stopRobot, getVersion } from './robot_api'
import JqxTextArea from './assets/jqwidgets-react/react_jqxtextarea';

class Misc extends Component {
    componentDidMount() {
        getVersion().then(response => response.text())
            .then((version) => {
                this.refs.version.setValue(version)
            }).catch(error => console.error(error));

    }
    render() {
        return (
            <Grid>
                <Row className="show-grid">
                    <Col xs={6} md={6}>
                        <table>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td><ControlButton imgSrc="arrow-orange-up-300px.png"
                                        onClick={() => moveForward()} /></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><ControlButton imgSrc="arrow-go-left-300px.png"
                                        onClick={() => turnLeft()} /></td>
                                    <td><ControlButton imgSrc="double-note-300px.png"
                                        onClick={() => playNote()} /></td>
                                    <td><ControlButton imgSrc="arrow-go-right-300px.png"
                                        onClick={() => turnRight()} /></td>
                                    <td></td>
                                    <td><ControlButton imgSrc="Stop-Sign-300px.png"
                                        onClick={() => stopRobot()} /></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><ControlButton imgSrc="arrow-orange-down-300px.png"
                                        onClick={() => moveBackward()} /></td>
                                </tr>
                                <tr>
                                </tr>
                            </tbody>
                        </table>
                    </Col>
                </Row>
                <Row className="show-grid">
                    <Col xs={12} md={6}>
                        <Leds />
                    </Col>
                </Row>
                <Row className="show-grid">
                    <Col xs={2} md={2}>
                        <button type="button" className="btn btn-danger">Shutdown</button>
                    </Col>
                </Row>
                <Row className="show-grid">
                    <Col xs={3} md={2}>
                        <Version ref="version" theme='dark' />
                    </Col>
                </Row>
            </Grid>
        );
    }
}

class Version extends Component {
    constructor(props) {
        super(props)
        this.state = { version: "" }
    }
    render() {
        return <JqxTextArea ref="status" theme='dark' value={"Version: " + this.state.version} />
    }
    setValue(value) {
        this.setState({ version: value })
    }
}

export default Misc;