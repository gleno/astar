import React, { Component } from 'react';
import JqxToggleButton from './assets/jqwidgets-react/react_jqxtogglebutton';

import { setLeds } from './robot_api'
class Leds extends Component {

    componentDidMount() {
        this.red.on('click', this.setLeds.bind(this))
        this.green.on('click', this.setLeds.bind(this))
        this.yellow.on('click', this.setLeds.bind(this))
    }
    render() {
        const style = { display: "inline-block", marginLeft: "10px", marginRight: "10px" }
        return (<div> LEDs
            <JqxToggleButton width={50} height={20} toggled="true" template='danger' theme='dark' style={style}
                ref={ref => this.red = ref} />
            <JqxToggleButton width={50} height={20} toggled="true" template='success' theme='dark' style={style}
                ref={ref => this.green = ref} />
            <JqxToggleButton width={50} height={20} toggled="true" template='warning' theme='dark' style={style}
                ref={ref => this.yellow = ref}
            />
        </div>)
    }

    setLeds() {
        setLeds(!this.red.toggled(), !this.yellow.toggled(), !this.green.toggled())
    }
}
export default Leds;