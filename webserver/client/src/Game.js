import React, { Component } from 'react';
import { Col, Grid, Row } from 'react-bootstrap';
import ControlButton from './ControlButton';
import JqxKanban, { jqx } from './assets/jqwidgets-react/react_jqxkanban';
import { stopRobot, moveForward, moveBackward, turnLeft, turnRight, playNote } from './robot_api'
import './game.css'

class Game extends Component {
    constructor(props) {
        super(props)
        this.interrupted = false;
        this.playing = false;
    }
    render() {
        return (
            <Grid>
                <Row className="show-grid">
                    <Col xs={6} md={2} className="pull-left">
                        <table>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td><ControlButton imgSrc="arrow-orange-up-300px.png"
                                        onClick={e => this.addAction("up")} ref="up" /></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><ControlButton imgSrc="arrow-go-left-300px.png"
                                        onClick={e => this.addAction("left")} ref="left" /></td>
                                    <td><ControlButton imgSrc="double-note-300px.png"
                                        onClick={e => this.addAction("note")} ref="note" /></td>
                                    <td><ControlButton imgSrc="arrow-go-right-300px.png"
                                        onClick={e => this.addAction("right")} ref="right" /></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><ControlButton imgSrc="arrow-orange-down-300px.png"
                                        onClick={e => this.addAction("down")} ref="down" /></td>
                                </tr>
                                <tr>
                                    <td><ControlButton imgSrc="Delete-Button-300px.png"
                                        onClick={this.clear.bind(this)} /></td>
                                    <td><ControlButton imgSrc="round-green-play-button-on-300px.png"
                                        onClick={this.play.bind(this)} ref="play" /></td>
                                    <td><ControlButton imgSrc="Stop-Sign-300px.png"
                                        onClick={this.stop.bind(this)} /></td>
                                </tr>
                            </tbody>
                        </table>
                    </Col>
                    <Col xs={6} md={2}>
                        <Kanban ref="kanban" />
                    </Col>
                </Row>
            </Grid>
        );
    }
    addAction(action) {
        var kanban = this.refs.kanban.refs.kanban
        kanban.addItem({ status: "new", resourceId: action, color: "#3c87a8" })

        var items = kanban.getItems()
        var maxItems = kanban.getColumn('new').maxItems
        this.disableActions(items.length >= maxItems);
    }

    disableActions(disabled) {
        this.refs.up.setOptions({ disabled: disabled })
        this.refs.down.setOptions({ disabled: disabled })
        this.refs.left.setOptions({ disabled: disabled })
        this.refs.right.setOptions({ disabled: disabled })
        this.refs.note.setOptions({ disabled: disabled })
    }

    clear() {
        this.stop()
        var kanban = this.refs.kanban.refs.kanban
        var items = kanban.getItems()
        items.forEach(function (entry) {
            if (entry)
                kanban.removeItem(entry.id);
        });
        this.disableActions(false);
    }

    play() {
        if (!this.playing) {
            this.playing = true
            this.disableActions(true)
            this.refs.play.setOptions({ disabled: this.playing })
            this.playProgram(0);
        }
    }

    stop() {
        this.interrupted = true
        stopRobot()
    }

    playProgram(index) {
        var kanban = this.refs.kanban.refs.kanban
        var items = kanban.getItems()
        if (items.length > index - 1 && index > 0) {
            kanban.updateItem(items[index - 1].id, { color: "#3c87a8" });
        }
        if (items.length > index && !this.interrupted) {
            var sleep_time = this.doAction(items[index].resourceId);
            kanban.updateItem(items[index].id, { color: "#ff7878" });
            setTimeout(this.playProgram.bind(this), sleep_time, index + 1);
        } else {
            this.interrupted = false
            this.playing = false
            this.refs.play.setOptions({ disabled: this.playing })
            this.disableActions(false);
        }
    }

    doAction(resourceId) {
        if (resourceId === "up") {
            moveForward();
            return 3000;
        } else if (resourceId === "down") {
            moveBackward();
            return 3000;
        } else if (resourceId === "left") {
            turnLeft();
            return 4000;
        } else if (resourceId === "right") {
            turnRight();
            return 4000;
        } else if (resourceId === "note") {
            playNote();
            return 1000;
        }
        return 4000;
    }

}

class Kanban extends Component {
    render() {
        var fields = [
            { name: "resourceId", type: "number" },
            { name: "status", map: "state", type: "string" },
            { name: "id", type: "string" },
            { name: "color", map: "hex", type: "string" },
        ];
        var source =
        {
            localData: [
                { state: "new", resourceId: "up", id: "0", hex: "#3c87a8" },
            ],
            dataType: "array",
            dataFields: fields
        };
        var dataAdapter = new jqx.dataAdapter(source);
        var resourcesAdapterFunc = function () {
            var resourcesSource =
            {
                localData: [
                    { id: "up", name: "Move Forward", image: "assets/images/arrow-orange-up-300px.png" },
                    { id: "down", name: "Move Backward", image: "assets/images/arrow-orange-down-300px.png" },
                    { id: "left", name: "Turn Left", image: "assets/images/arrow-go-left-300px.png" },
                    { id: "right", name: "Turn Right", image: "assets/images/arrow-go-right-300px.png" },
                    { id: "note", name: "Play Note", image: "assets/images/double-note-300px.png" },
                ],
                dataType: "array",
                dataFields: [
                    { name: "id", type: "number" },
                    { name: "name", type: "string" },
                    { name: "image", type: "string" },
                ]
            };
            var resourcesDataAdapter = new jqx.dataAdapter(resourcesSource);
            return resourcesDataAdapter;
        }

        let template = `<div class='jqx-kanban-item' id='' >
        <div class='jqx-kanban-item-color-status'></div>
        <div class='jqx-kanban-item-avatar'></div>
        </div>`
        return (
            <JqxKanban template={template} width='100%' height='600'
                theme="dark"
                ref="kanban"
                resources={resourcesAdapterFunc()}
                source={dataAdapter}
                columns={[
                    { text: "Program", dataField: "new", maxItems: 12 }
                ]} />
        )
    }
}

export default Game;