import React, { Component } from 'react';
import { setMotors } from './robot_api'

const style = {
    border: '1px solid black',
    backgroundColor: 'gray',
    width: '14em',
    height: '14em',
    marginLeft: 'auto',
    marginRight: 'auto',
};

export default class Joystick extends Component {
    constructor(props) {
        super(props);
        this.stop_motors = true
        this.block_set_motors = false
        this.mouse_dragging = false
        this.state = { text: "0, 0" }
    }

    render() {
        return <div style={style} ref={ref => this.joystickRef = ref}
            onMouseMove={this.handleMouseMove.bind(this)}
            onTouchMove={this.handleTouchMove.bind(this)}
            onTouchStart={this.handleTouchStart.bind(this)}
            onTouchEnd={this.handleTouchEnd.bind(this)}
            onMouseDown={this.handleMouseDown.bind(this)}
            onMouseUp={this.handleMouseUp.bind(this)}
        >
            {this.state.text}
        </div>
    }

    handleTouchStart(e) {
        this.mouse_dragging = true
        this.handleTouchMove(e);
    }

    handleTouchMove(e) {
        e.preventDefault();
        let touch = e.touches[0] || e.changedTouches[0];
        this.dragTo(touch.pageX, touch.pageY);
    }

    handleTouchEnd(e) {
        this.handleMouseUp(e);
    }

    handleMouseDown(e) {
        e.preventDefault();
        this.mouse_dragging = true
        this.dragTo(e.pageX, e.pageY);
    }

    handleMouseUp(e) {
        if (this.mouse_dragging) {
            e.preventDefault();
            this.mouse_dragging = false
            this.setMotors(0, 0);
        }
    }

    handleMouseMove(e) {
        if (this.mouse_dragging) {
            e.preventDefault();
            this.dragTo(e.pageX, e.pageY);
        }
    }

    dragTo(x, y) {
        // TODO this works only of there is one Joystick. this shold be cleaned and replaced with
        // a dynamic ref using createRef
        var elm = this.joystickRef.getBoundingClientRect();

        x = x - elm.left;
        y = y - elm.top;
        var w = elm.width
        var h = elm.height

        x = (x - w / 2.0) / (w / 2.0);
        y = (y - h / 2.0) / (h / 2.0);

        if (x < -1) x = -1;
        if (x > 1) x = 1;
        if (y < -1) y = -1;
        if (y > 1) y = 1;

        var max = 1;
        var left_motor = max * (-y + x);
        var right_motor = max * (-y - x);

        if (left_motor > max) left_motor = max;
        if (left_motor < -max) left_motor = -max;

        if (right_motor > max) right_motor = max;
        if (right_motor < -max) right_motor = -max;

        this.setMotors(left_motor, right_motor);
    }

    setMotors(left, right) {
        if (left === 0 && right === 0) {
            this.stop_motors = true
        }
        this.setState({ text: left + " " + right })
        if (this.block_set_motors) return;
        this.stop_motors = false
        this.block_set_motors = true
        setMotors(left, right).then(() => { this.setMotorsDone() })
    }

    setMotorsDone() {
        this.block_set_motors = false
        if (this.stop_motors) {
            this.setMotors(0, 0);
        }
    }
}