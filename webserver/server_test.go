package webserver

import (
	"flag"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/gorilla/websocket"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gleno/astar/commands"
	"gitlab.com/gleno/astar/drivers"
	"gitlab.com/gleno/astar/mock_drivers"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"syscall"
	"testing"
	"time"
)

var testOnDevice = flag.Bool("test-on-device", false, "Tells that test are actually run on device")

func TestMain(m *testing.M) {
	gin.SetMode(gin.TestMode)
	os.Exit(m.Run())
}

func TestRestApis(t *testing.T) {
	testCases := []struct {
		name            string
		method          string
		api             string
		expectedStatus  int
		expectedContent string
		expect          func(m *mock_drivers.MockAStar)
	}{
		{"Index", "GET", "/", http.StatusOK, "title>Rominet Control Panel</title>", nil},

		{"Led", "PUT", "/api/leds/red/true/yellow/true/green/true", http.StatusOK, "{}",
			func(m *mock_drivers.MockAStar) { m.EXPECT().Leds(true, true, true) }},
		{"LedInvalid", "PUT", "/api/leds/red/aaa/yellow/aaa/green/aaa", http.StatusBadRequest, "error", nil},

		{"Motors", "PUT", "/api/motors/left/1/right/1", http.StatusOK, "{}",
			func(m *mock_drivers.MockAStar) { m.EXPECT().Motors(1., 1.) }},
		{"MotorsInvalid1", "PUT", "/api/motors/left/2/right/1", http.StatusBadRequest, "error", nil},
		{"MotorsInvalid2", "PUT", "/api/motors/left/aaa/right/1", http.StatusBadRequest, "error", nil},

		{"resetOdometry", "POST", "/api/reset_odometry", http.StatusOK, "{}", nil},

		{"Rotate", "PUT", "/api/rotate/angle/1/speed/1", http.StatusOK, "{}",
			func(m *mock_drivers.MockAStar) {
				m.EXPECT().ReadEncoders().AnyTimes().Return(0, 0)
				m.EXPECT().Motors(gomock.Any(), gomock.Any()).MinTimes(0)
			}},
		{"Rotate", "PUT", "/api/rotate/angle/-1/speed/1", http.StatusOK, "{}",
			func(m *mock_drivers.MockAStar) {
				m.EXPECT().ReadEncoders().AnyTimes().Return(0, 0)
				m.EXPECT().Motors(gomock.Any(), gomock.Any()).MinTimes(0)
			}},
		{"RotateInvalid1", "PUT", "/api/rotate/angle/1/speed/2", http.StatusBadRequest, "error", nil},
		{"RotateInvalid2", "PUT", "/api/rotate/angle/aaa/speed/1", http.StatusBadRequest, "error", nil},

		{"Move", "PUT", "/api/move/distance/-1/speed/1", http.StatusOK, "{}",
			func(m *mock_drivers.MockAStar) {
				m.EXPECT().ReadEncoders().AnyTimes().Return(0, 0)
				m.EXPECT().Motors(gomock.Any(), gomock.Any()).MinTimes(0)
			}},
		{"MoveInvalid1", "PUT", "/api/move/distance/-1/speed/2", http.StatusBadRequest, "error", nil},
		{"MoveInvalid2", "PUT", "/api/move/distance/aaa/speed/1", http.StatusBadRequest, "error", nil},

		{"Status", "GET", "/api/status", http.StatusOK, "{\"Battery\":7",
			func(m *mock_drivers.MockAStar) {
				m.EXPECT().ReadEncoders().AnyTimes().Return(0, 0)
				m.EXPECT().ReadBattery().Return(7.)
				m.EXPECT().ReadButtons().Return(true, true, true)
			}},
		{"Status", "GET", "/api/camera", http.StatusTemporaryRedirect, "", nil},
		{"Version", "GET", "/api/version", http.StatusOK, "undefined", nil},
		{"WsPort", "GET", "/api/wsport", http.StatusOK, "3333", nil},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()
			mockAstar := mock_drivers.NewMockAStar(mockCtrl)
			mockAstar.EXPECT().ReadEncoders().Return(0, 0)

			if tc.expect != nil {
				tc.expect(mockAstar)
			}
			r := GetRouter(mockAstar, 3333)

			req, _ := http.NewRequest(tc.method, tc.api, nil)
			testHTTPResponse(t, r, req, tc.expectedStatus, tc.expectedContent)
		})
	}
}

func TestGetAstar(t *testing.T) {
	astar := GetAstar()
	if !*testOnDevice {
		assert.IsType(t, drivers.NewFakeAStar(), astar, "On a non-device HW, we expect to get a FakeAstar")
	} else {
		assert.IsType(t, drivers.AStarDriver{}, astar, "On a non-device HW, we expect to get a FakeAstar")
	}
}

// Helper function to process a request and test its response
func testHTTPResponse(t *testing.T, r *gin.Engine, req *http.Request, expectedStatus int, expectedMsg string) {
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	if w.Code != expectedStatus {
		t.Fatal(fmt.Sprintf("Status message is not correct. Expected: %d, got: %d", expectedStatus, w.Code))
	}
	p, err := ioutil.ReadAll(w.Body)
	if err != nil {
		t.Fatal(err.Error())
	}
	if strings.Index(string(p), expectedMsg) < 0 {
		t.Fatal(fmt.Sprintf("Missing content '%s' in '%s'", expectedMsg, p))
	}
}

// Helper function which returns a websocket.Conn connected to a server serving handler
// cleanup shall be called by the caller to cleanup resources
func getTestConn(t *testing.T, handler http.HandlerFunc) (ws *websocket.Conn, cleanup func()) {
	server := httptest.NewServer(http.HandlerFunc(handler))

	u := "ws" + strings.TrimPrefix(server.URL, "http")
	ws, _, err := websocket.DefaultDialer.Dial(u, nil)

	if err != nil {
		t.Fatal("Cannot dial", err)
	}
	return ws, func() {
		ws.Close()
		server.Close()
	}
}

func TestWebsocketStatus(t *testing.T) {
	astar := GetAstar()
	serverdata := &serverData{commands.NewRobot(astar), 3333}

	ws, cleanup := getTestConn(t, serverdata.wsStatus)
	defer cleanup()

	defer ws.WriteMessage(websocket.TextMessage, []byte("stop"))

	testTimeout := time.AfterFunc(time.Second, func() {
		t.Fatal("No message received")
	})
	err := ws.WriteMessage(websocket.TextMessage, []byte("s"))
	if err != nil {
		t.Fatal("Cannot write", err)
	}
	var statusMessage RobotStatus
	err = ws.ReadJSON(&statusMessage)
	testTimeout.Stop()
	if err != nil {
		t.Fatal("Cannot decode json", err)
	}

	if (statusMessage == RobotStatus{}) {
		t.Fatal("Received status was not correct")
	}
}

func TestWebsocketStatusStop(t *testing.T) {
	astar := GetAstar()
	serverdata := &serverData{commands.NewRobot(astar), 3333}

	ws, cleanup := getTestConn(t, serverdata.wsStatus)
	defer cleanup()

	testTimeout := time.AfterFunc(time.Second, func() {
		t.Fatal("No message received")
	})

	ws.WriteMessage(websocket.TextMessage, []byte("stop"))

	_, _, err := ws.ReadMessage()
	if err == nil {
		t.Fatal("Shoud not be able to write", err)
	}
	testTimeout.Stop()
}

func TestListenAndServeInterrupted(t *testing.T) {
	exited := make(chan bool)
	go func() {
		ListenAndServe(8080)
		exited <- true
	}()

	// Wait for ListenAndServe to actually be running
	time.Sleep(time.Millisecond * 100)

	p, err := os.FindProcess(syscall.Getpid())
	if err != nil {
		t.Fatal("Cannot get the current process's PID")
	}
	p.Signal(os.Interrupt)

	select {
	case <-exited:
	case <-time.After(1 * time.Second):
		t.Fatalf("Timeout waiting for ListenAndServe to exit")
	}
}
