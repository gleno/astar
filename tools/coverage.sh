#!/bin/bash -e
#
# Code coverage generation
# Create a coverage file for each package
echo 'mode: count' > coverage.txt
for package in `go list gitlab.com/gleno/...`; do
    go test -covermode=count -coverprofile=profile.out -coverpkg=gitlab.com/gleno/... "$package"
    if [ -f profile.out ]; then
        tail -q -n +2  profile.out >> coverage.txt
        rm profile.out
    fi
done

# Display the global code coverage
go tool cover -func=coverage.txt

if [ -n "$CODECOV_TOKEN" ]; then 
    bash <(curl -s https://codecov.io/bash) -t $CODECOV_TOKEN
fi