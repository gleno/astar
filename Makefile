PROJECT_NAME := "astar"
PKG := "gitlab.com/gleno/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/...)
GO_FILES := $(shell find . -name '*.go' | grep -v _test.go)
GO_PATH := $(shell ${GO_PATH:-~/go})
VERSION := $(shell git log -1 --pretty=%h)
BUILD_FLAGS = -ldflags "-X gitlab.com/gleno/astar/webserver.version=${VERSION}"

.PHONY: all dep build build_pi clean test coverage lint client_ui snap

all: build build_pi

lint:
	@golint -set_exit_status ${PKG_LIST}

test:
	@go test -v ${PKG_LIST}

test-report:
	@go test ${PKG_LIST} 2>&1 | go-junit-report > report.xml

race: dep
	@go test -race -short ${PKG_LIST}

msan: dep
	@go test -msan -short ${PKG_LIST}

coverage:
	./tools/coverage.sh;

dep:
	@go get -v -d ./...
	@mkdir -p build/bin/ build/bin/linux_arm

build: dep client_ui
	packr build ${BUILD_FLAGS} -o build/bin/astar gitlab.com/gleno/astar/main

build_pi: dep client_ui
	GOARCH=arm GOOS=linux GOARM=7 packr build ${BUILD_FLAGS} -o build/bin/linux_arm/astar gitlab.com/gleno/astar/main

cyclo:
	@gocyclo -over 15 .

clean: ## Remove previous build
	@rm -f build

help:
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

client_ui:
	$(MAKE) -C webserver/client all