package drivers_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/gleno/astar/drivers"
	"math"
	"testing"
	"time"
)

func TestFakeAstar(t *testing.T) {
	f := drivers.NewFakeAStar()
	f.Motors(1, 1)
	time.Sleep(100 * time.Millisecond)

	left, right := f.ReadEncoders()
	assert.True(t, math.Abs(float64(left)) > 1)
	assert.True(t, math.Abs(float64(right)) > 1)

	f.Motors(-1, -1)
	time.Sleep(100 * time.Millisecond)
	left, right = f.ReadEncoders()

	assert.True(t, math.Abs(float64(left)) < 100)
	assert.True(t, math.Abs(float64(right)) < 100)

}
