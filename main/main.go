package main

import (
	"flag"
	"gitlab.com/gleno/astar"
	"gitlab.com/gleno/astar/webserver"
	"log"
)

func main() {
	configFile := flag.String("configFile", "robot.ini", "The configuration file")
	flag.Parse()

	config := astar.NewConfig(*configFile)

	port := config.Section("ui").Key("port").MustInt(8080)
	webHookChatBot := config.Section("slack").Key("webHookChatBot").String()
	if len(webHookChatBot) > 0 {
		bot := webserver.NewChatBot(config, webHookChatBot)
		err := bot.PostStartUpMessage(port)
		if err != nil {
			log.Print("Could not post message to Slack", err)
		}
	}
	webserver.ListenAndServe(port)
}
