// +build

package commands_test

import (
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gleno/astar/commands"
	"gitlab.com/gleno/astar/drivers"
	"gitlab.com/gleno/astar/mock_drivers"

	"testing"
	"time"
)

func TestBasicAPIs(t *testing.T) {

	testCases := []struct {
		name   string
		expect func(m *mock_drivers.MockAStar, robot *commands.Robot)
	}{
		{"Led", func(m *mock_drivers.MockAStar, robot *commands.Robot) {
			m.EXPECT().Leds(true, true, true)
			robot.Leds(true, true, true)
		}},
		{"Motors", func(m *mock_drivers.MockAStar, robot *commands.Robot) {
			m.EXPECT().Motors(1., 1.)
			robot.Speed(1., 1.)
		}},
		{"Notes", func(m *mock_drivers.MockAStar, robot *commands.Robot) {
			m.EXPECT().PlayNotes("aaa")
			robot.PlayNotes("aaa")
		}},
		{"Battery", func(m *mock_drivers.MockAStar, robot *commands.Robot) {
			m.EXPECT().ReadBattery()
			robot.ReadBattery()
		}},
		{"Buttons", func(m *mock_drivers.MockAStar, robot *commands.Robot) {
			m.EXPECT().ReadButtons()
			robot.ReadButtons()
		}},
		{"Analog", func(m *mock_drivers.MockAStar, robot *commands.Robot) {
			m.EXPECT().ReadAnalog()
			robot.ReadAnalog()
		}},
		{"Encoders", func(m *mock_drivers.MockAStar, robot *commands.Robot) {
			m.EXPECT().ReadEncoders().MinTimes(1)
			robot.ReadEncoders()
		}},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()
			mockAstar := mock_drivers.NewMockAStar(mockCtrl)
			mockAstar.EXPECT().ReadEncoders()
			robot := commands.NewRobot(mockAstar)

			if tc.expect != nil {
				tc.expect(mockAstar, robot)
			}
		})
	}
}

func TestRotate(t *testing.T) {
	simu := drivers.NewFakeAStar()
	robot := commands.NewRobot(simu)
	robot.Rotate(1., 1.)
	time.Sleep(80 * time.Millisecond)
	assert.InDelta(t, robot.ReadSituation().Distance, 0, 0.001)
	assert.True(t, robot.ReadSituation().Yaw > 0.1)
	robot.Speed(0, 0)
}

func TestMove(t *testing.T) {
	simu := drivers.NewFakeAStar()
	robot := commands.NewRobot(simu)
	robot.Move(1., 1.)
	time.Sleep(100 * time.Millisecond)
	l, r := robot.LastMotorsCommands()
	assert.True(t, l > 0.2)
	assert.True(t, r > 0.2)
	robot.Speed(0, 0)
}

func TestInterruptCommand(t *testing.T) {
	simu := drivers.NewFakeAStar()
	robot := commands.NewRobot(simu)
	robot.Move(1., 1.)
	time.Sleep(100 * time.Millisecond)
	l, r := robot.LastMotorsCommands()
	assert.True(t, l > 0.1)
	assert.True(t, r > 0.1)

	robot.Speed(0., 0.)
	time.Sleep(50 * time.Millisecond)
	l, r = robot.LastMotorsCommands()
	assert.Equal(t, 0., l)
	assert.Equal(t, 0., r)
}

func TestReplaceCommand(t *testing.T) {
	simu := drivers.NewFakeAStar()
	robot := commands.NewRobot(simu)
	robot.Move(1., 1.)
	time.Sleep(50 * time.Millisecond)
	l, r := robot.LastMotorsCommands()
	assert.True(t, l > 0.1)
	assert.True(t, r > 0.1)

	robot.Rotate(0., 0.)
	time.Sleep(50 * time.Millisecond)
	l, r = robot.LastMotorsCommands()
	assert.Equal(t, 0., l)
	assert.Equal(t, 0., r)
}
