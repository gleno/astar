// The tests take too long under the race detector.

package commands

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/gleno/astar/drivers"
	"testing"
	"time"
)

func TestSquare(t *testing.T) {
	simu := drivers.NewFakeAStar()

	robot := NewRobot(simu)
	// The default YawAccuracy is too strong when running on gitlab CI runnner.
	motionCmd := NewSquare(robot, 0.2, 1, YawAccuracy(0.05))
	motionCmd.Start()
	testTimeout := time.AfterFunc(10*time.Second, func() {
		t.Fatal("Command was not finished in time")
	})
	result := <-motionCmd.Done
	testTimeout.Stop()
	assert.True(t, result, "Command was not successful")
	assert.InDelta(t, robot.ReadSituation().Distance, 0.8, 0.01)
}
