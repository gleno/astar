package commands

import (
	"fmt"
	"gitlab.com/gleno/astar"
	"math"
	"sync"
	"time"
)

// Options confirgures some aspects of MotionCommand execution
type Options struct {
	TickerDuration                time.Duration
	InactivityTimeout             time.Duration
	YawAccuracy, DistanceAccuracy float64
}

// OptionSetter is a function
type OptionSetter func(*Options)

// TickerDuration sets the value of the ticker
func TickerDuration(tickerDuration time.Duration) OptionSetter {
	return func(args *Options) {
		args.TickerDuration = tickerDuration
	}
}

// InactivityTimeout sets the value of the ticker
func InactivityTimeout(inactivityTimeout time.Duration) OptionSetter {
	return func(args *Options) {
		args.InactivityTimeout = inactivityTimeout
	}
}

// YawAccuracy sets the yaw window to stop the command
func YawAccuracy(yawAccuracy float64) OptionSetter {
	return func(args *Options) {
		args.YawAccuracy = yawAccuracy
	}
}

// DistanceAccuracy sets the distance window to stop the command
func DistanceAccuracy(distanceAccuracy float64) OptionSetter {
	return func(args *Options) {
		args.DistanceAccuracy = distanceAccuracy
	}
}

// MotionCommand bla
type MotionCommand struct {
	robot                             *Robot
	Done                              chan bool
	stop                              chan int
	initialSituation                  astar.Situation
	lastLeftCommand, lastRightCommand float64
	mover                             mover
	waitingForMotorsToStop            bool
	returnStatus                      sync.Once
	stopOnce                          sync.Once
	options                           *Options
}

type mover interface {
	move() bool
}

// NewMotionCommand creates a new MotionCommand. Opts allows to configure command behavior
func NewMotionCommand(r *Robot, opts ...OptionSetter) *MotionCommand {
	// Default Options
	args := &Options{
		TickerDuration:    10 * time.Millisecond,
		InactivityTimeout: time.Second,
		YawAccuracy:       .001,
		DistanceAccuracy:  .001,
	}

	for _, opt := range opts {
		opt(args)
	}
	return &MotionCommand{
		robot:                  r,
		Done:                   make(chan bool),
		stop:                   make(chan int),
		initialSituation:       r.ReadSituation(),
		waitingForMotorsToStop: false,
		returnStatus:           sync.Once{},
		stopOnce:               sync.Once{},
		options:                args,
	}
}

// Start begins the execution of the MotionCommand asynchronously
func (m *MotionCommand) Start() {
	ticker := time.NewTicker(m.options.TickerDuration)
	inactivityTimeout := time.NewTicker(m.options.InactivityTimeout)
	lastSituation := m.initialSituation
	go func() {
		mover := m.mover
		for {
			select {
			case <-ticker.C:
				if mover == nil || m.mover.move() {
					ticker.Stop()
					inactivityTimeout.Stop()
					m.sendDoneStatus(true)
					mover = nil
				}
			case <-m.stop:
				if mover != nil {
					ticker.Stop()
					inactivityTimeout.Stop()
					m.sendDoneStatus(false)
					mover = nil
					break
				}
			case <-inactivityTimeout.C:
				currentSituation := m.robot.ReadSituation()
				if currentSituation.X == lastSituation.X &&
					currentSituation.Y == lastSituation.Y &&
					currentSituation.Yaw == lastSituation.Yaw {
					fmt.Println("Nothing moved since one second, stopping")
					go m.Stop()
				}
				lastSituation = currentSituation
			}
		}
	}()
}

func (m *MotionCommand) sendDoneStatus(status bool) {
	go m.returnStatus.Do(func() {
		select {
		case m.Done <- status:
		}
	})
}

// Stop interrupts the current command. When called, the Done channel returns false
func (m *MotionCommand) Stop() {
	m.stopOnce.Do(func() {
		m.stop <- 1
	})
}

const maxAccelerationCommand = 0.05

func capAcceleration(lastCommand, cmd float64) float64 {
	if math.Abs(lastCommand-cmd) > maxAccelerationCommand {
		return lastCommand + math.Copysign(maxAccelerationCommand, cmd-lastCommand)
	}
	return cmd
}
