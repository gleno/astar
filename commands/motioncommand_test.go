package commands

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/gleno/astar/drivers"
	"testing"
	"time"
)

func TestStart(t *testing.T) {
	motionCmd := NewMotionCommand(NewRobot(drivers.NewFakeAStar()))
	motionCmd.Start()
	testTimeout := time.AfterFunc(100*time.Millisecond, func() {
		t.Fatal("Command was not finished in time")
	})
	<-motionCmd.Done
	testTimeout.Stop()
}

func TestStop(t *testing.T) {
	motionCmd := NewMotionCommand(NewRobot(drivers.NewFakeAStar()))
	mover := struct{ ProtoMover }{}
	mover.moveMethod = func() bool { return false }
	motionCmd.mover = mover
	motionCmd.Start()
	motionCmd.Stop()
	testTimeout := time.AfterFunc(50*time.Millisecond, func() {
		t.Fatal("Command should have Stopped immediately")
	})

	assert.False(t, <-motionCmd.Done, "command should have returned false")
	testTimeout.Stop()
}

type ProtoMover struct{ moveMethod func() bool }

func (t ProtoMover) move() bool { return t.moveMethod() }

func TestInactivity(t *testing.T) {
	motionCmd := NewMotionCommand(NewRobot(drivers.NewFakeAStar()), InactivityTimeout(100*time.Millisecond))
	mover := struct{ ProtoMover }{}
	mover.moveMethod = func() bool { return false }
	motionCmd.mover = mover
	motionCmd.Start()

	testTimeout := time.AfterFunc(200*time.Millisecond, func() {
		t.Fatal("Inactivity timeout did not kick in time")
	})
	assert.False(t, <-motionCmd.Done, "Command should have returned false")
	testTimeout.Stop()
}
