// The tests take too long under the race detector.

package commands

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/gleno/astar/drivers"
	"testing"
	"time"
)

func TestTurn(t *testing.T) {
	simu := drivers.NewFakeAStar()
	robot := NewRobot(simu)
	desiredYaw := 10
	motionCmd := NewTurn(robot, 10, 1, TickerDuration(50*time.Millisecond))
	motionCmd.Start()
	testTimeout := time.AfterFunc(10*time.Second, func() {
		t.Fatal("Command was not finished in time")
	})
	result := <-motionCmd.Done
	testTimeout.Stop()
	assert.True(t, result, "Command was not successful")
	assert.InDelta(t, robot.ReadSituation().UnboundedYaw, desiredYaw, 0.5)
}
