package commands

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/gleno/astar/drivers"
	"testing"
	"time"
)

func TestMoveStraight(t *testing.T) {
	simu := drivers.NewFakeAStar()
	robot := NewRobot(simu)
	distanceToMove := 0.1
	motionCmd := NewMoveStraight(robot, distanceToMove, 1)
	motionCmd.Start()
	testTimeout := time.AfterFunc(1000*time.Millisecond, func() {
		t.Fatalf("Command was not finished in time. Robot was at distance: %f",
			robot.ReadSituation().Distance)
	})
	result := <-motionCmd.Done
	testTimeout.Stop()
	assert.True(t, result, "Command was not successful")
	assert.InDelta(t, robot.ReadSituation().Distance, distanceToMove, 0.001)
}
