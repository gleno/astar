package commands

import (
	"log"
	"math"
)

type square struct {
	*MotionCommand
	length       float64
	maxSpeed     float64
	commands     []int
	currentMover *MotionCommand
	opts         []OptionSetter
}

const (
	move = iota
	rotate
)

// NewSquare creates a new MotionCommand which makes the robot follow a square.
// opts allows to configure behavior
func NewSquare(r *Robot, length float64, maxSpeed float64, opts ...OptionSetter) *MotionCommand {
	motionCommand := NewMotionCommand(r, opts...)
	square := &square{motionCommand,
		length,
		maxSpeed,
		[]int{move, rotate, move, rotate, move, rotate, move, rotate},
		nil,
		opts}
	motionCommand.mover = square
	square.currentMover = square.getNextCommand()
	return square.MotionCommand
}

func (m *square) move() bool {
	if m.currentMover != nil {
		if m.currentMover.mover.move() {
			m.currentMover = m.getNextCommand()
		}
		return false
	}
	return true
}

func (m *square) getNextCommand() *MotionCommand {
	var cmd *MotionCommand
	if len(m.commands) > 0 {
		switch m.commands[0] {
		case move:
			log.Print("Square picks next commands: MoveStraight")
			cmd = NewMoveStraight(m.robot, m.length, m.maxSpeed, m.opts...)
		case rotate:
			log.Print("Square picks next commands: Turn")
			cmd = NewTurn(m.robot, math.Pi/2, m.maxSpeed/2, m.opts...)
		default:
			cmd = nil
		}
		m.commands = m.commands[1:]
	}
	return cmd
}
