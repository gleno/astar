package commands

import (
	"time"
)

// PID represents a PID Controller (https://en.wikipedia.org/wiki/PID_controller)
type PID struct {
	P, I, D, errorIntergation, errorDerivative, errorValue, previousErrorValue float64
	lastTime                                                                   time.Time
}

// NewPID creates a PID
func NewPID(P, I, D float64) *PID {
	return &PID{
		P: P,
		I: I,
		D: D,
	}
}

// GetOutput returns the next value from the PID
func (p *PID) GetOutput(setPoint, readValue float64, currentTime time.Time) float64 {
	step := 0.
	if !p.lastTime.IsZero() {
		step = currentTime.Sub(p.lastTime).Seconds()
	} else {
		step = 0
	}

	p.previousErrorValue = p.errorValue
	p.errorValue = setPoint - readValue
	p.errorIntergation += p.errorValue * step
	if step != 0 {
		p.errorDerivative = (p.errorValue - p.previousErrorValue) / step
	}

	p.lastTime = currentTime

	return (p.P*p.errorValue +
		p.I*p.errorIntergation +
		p.D*p.errorDerivative)
}
