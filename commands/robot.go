package commands

import (
	"gitlab.com/gleno/astar"
	"gitlab.com/gleno/astar/drivers"
	"sync"
)

// Robot represents a Romi board
type Robot struct {
	astar                               drivers.AStar
	odom                                *astar.Odometer
	encoder                             *astar.Encoders
	currentCmd                          *MotionCommand
	lastLeftMotorCmd, lastRightMotorCmd float64
	mutex                               *sync.Mutex
	optSetters                          []OptionSetter
}

// NewRobot creates a Robot
// opts defines the Options to use when creating MotionCommands
func NewRobot(a drivers.AStar, opts ...OptionSetter) *Robot {
	encoder := astar.NewEncoders(a)
	odom := astar.NewOdometer(encoder)
	r := &Robot{a, odom, encoder, nil, 0, 0, &sync.Mutex{}, opts}
	return r
}

// Leds sets led status using red ,yellow ,green params
func (r *Robot) Leds(red, yellow, green bool) { r.astar.Leds(red, yellow, green) }

// motors sets the motors values
func (r *Robot) motorsDirectAccess(left, right float64) {
	r.lastLeftMotorCmd, r.lastRightMotorCmd = left, right
	r.astar.Motors(left, right)
}

// LastMotorsCommands returns the last commands sent to the motors
// This function is only for test
func (r *Robot) LastMotorsCommands() (float64, float64) {
	return r.lastLeftMotorCmd, r.lastRightMotorCmd
}

// Speed sets the speed values
func (r *Robot) Speed(left, right float64) {
	r.startCmd(nil)
	r.motorsDirectAccess(left, right)
}

// PlayNotes plays a set of notess
func (r *Robot) PlayNotes(notes string) { r.astar.PlayNotes(notes) }

// ReadBattery returns the battery value
func (r *Robot) ReadBattery() float64 { return r.astar.ReadBattery() }

// ReadButtons returns the current buttons states
func (r *Robot) ReadButtons() (bool, bool, bool) { return r.astar.ReadButtons() }

// ReadAnalog returns the measured analog input from the 32u4
func (r *Robot) ReadAnalog() []int { return r.astar.ReadAnalog() }

// ReadEncoders reads the encoders values
func (r *Robot) ReadEncoders() (int, int) { return r.encoder.ReadEncoders() }

// Rotate rotates the Robot by angle at the given speed
func (r *Robot) Rotate(angle, speed float64) {
	r.startCmd(NewTurn(r, angle, speed, r.optSetters...))
}

// Move moves the Robot straight by distance at the given speed
func (r *Robot) Move(distance, speed float64) {
	r.startCmd(NewMoveStraight(r, distance, speed, r.optSetters...))
}

func (r *Robot) startCmd(newCmd *MotionCommand) {
	r.mutex.Lock()
	if r.currentCmd != nil {
		r.currentCmd.Stop()
	}
	r.currentCmd = newCmd
	r.mutex.Unlock()

	if newCmd != nil {
		newCmd.Start()
	}
}

// ReadSituation reads and returns the current Situation
func (r *Robot) ReadSituation() astar.Situation {
	return r.odom.ReadSituation()
}

// ResetOdometry resets the odometry to zero
func (r *Robot) ResetOdometry() {
	r.odom.ResetOdometry()
}
