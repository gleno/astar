package commands

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestPIDProportional(t *testing.T) {
	Kp := 0.1
	p := NewPID(Kp, 0, 0)
	setupPoint := 10.
	for i := 1.; i <= 10; i++ {
		output := p.GetOutput(setupPoint, i, time.Now())
		expected := (setupPoint - i) * Kp
		assert.Equal(t, output, expected)
	}
}

func TestPIDIntegration(t *testing.T) {
	p := NewPID(0, 0.1, 0)
	setupPoint := 10.
	measurement := 0.
	maxTime := 100
	startTime := time.Now()
	for i := 1; i <= maxTime; i++ {
		measurement = p.GetOutput(setupPoint, measurement, startTime.Add(time.Second*time.Duration(i)))
		assert.True(t, measurement < setupPoint)
		if i > 50 {
			assert.InDelta(t, setupPoint, measurement, 0.1)
		}
	}

}
