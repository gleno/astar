package commands

import (
	"math"
)

type turn struct {
	*MotionCommand
	yaw      float64
	maxSpeed float64
}

// NewTurn crestes a new MotionCommand which turns the robot in place.
// opts allows to configure behavior
func NewTurn(r *Robot, yaw float64, maxSpeed float64, opts ...OptionSetter) *MotionCommand {
	motionCommand := NewMotionCommand(r, opts...)
	turn := &turn{motionCommand, yaw, maxSpeed}
	motionCommand.mover = turn
	return turn.MotionCommand
}

func (m *turn) move() bool {
	situation := m.robot.ReadSituation()
	doneYaw := situation.UnboundedYaw - m.initialSituation.UnboundedYaw
	remainingYaw := m.yaw - doneYaw

	if m.waitingForMotorsToStop {
		return situation.SpeedLeft == 0 && situation.SpeedRight == 0
	} else if math.Abs(remainingYaw) < m.MotionCommand.options.YawAccuracy ||
		math.Abs(doneYaw)-math.Abs(m.yaw) > 0.01 {
		m.robot.motorsDirectAccess(0, 0)
		m.lastLeftCommand, m.lastRightCommand = 0, 0
		// Wait for the robot to stop to tell that we are done
		m.waitingForMotorsToStop = true
		// todo this should be done if we have reached this if once
		return situation.SpeedLeft == 0 && situation.SpeedRight == 0
	}
	cmd := m.getMotorCommand(remainingYaw, m.lastLeftCommand, m.maxSpeed)
	left := capAcceleration(m.lastLeftCommand, cmd)
	right := capAcceleration(m.lastRightCommand, -cmd)
	m.robot.motorsDirectAccess(left, right)
	m.lastLeftCommand, m.lastRightCommand = left, right
	return false
}

func (m *turn) getMotorCommand(remainingYaw, lastCommand, maxSpeed float64) float64 {
	var cmd float64
	maxSpeed = math.Abs(maxSpeed)
	lastCommand = math.Abs(lastCommand)

	var speedReductionYaw = 1 * (1 + maxSpeed - 0.5)
	if math.Abs(remainingYaw) > speedReductionYaw || lastCommand == 0 {
		cmd = maxSpeed
	} else {
		cmd = maxSpeed * math.Abs(remainingYaw) / speedReductionYaw
	}
	cmd = math.Min(cmd, 1)
	cmd = math.Copysign(cmd, remainingYaw)

	return cmd
}
