module gitlab.com/gleno/astar

require (
	github.com/eclipse/paho.mqtt.golang v1.1.1 // indirect
	github.com/gin-contrib/static v0.0.0-20181225054800-cf5e10bbd933
	github.com/gin-gonic/gin v1.3.0
	github.com/gobuffalo/packr v1.21.9
	github.com/golang/mock v1.1.1
	github.com/gopherjs/gopherjs v0.0.0-20181103185306-d547d1d9531e // indirect
	github.com/gorilla/websocket v1.4.0
	github.com/hashicorp/go-multierror v1.0.0 // indirect
	github.com/jtolds/gls v4.2.1+incompatible // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/rogpeppe/go-internal v1.1.0 // indirect
	github.com/sigurn/crc8 v0.0.0-20160107002456-e55481d6f45c // indirect
	github.com/sigurn/utils v0.0.0-20151230205143-f19e41f79f8f // indirect
	github.com/smartystreets/assertions v0.0.0-20180927180507-b2de0cb4f26d // indirect
	github.com/smartystreets/goconvey v0.0.0-20181108003508-044398e4856c // indirect
	github.com/stretchr/testify v1.3.0
	gobot.io/x/gobot v1.12.0
	golang.org/x/net v0.0.0-20190119204137-ed066c81e75e // indirect
	golang.org/x/sys v0.0.0-20190116161447-11f53e031339 // indirect
	gopkg.in/ini.v1 v1.41.0
	periph.io/x/periph v3.3.0+incompatible // indirect
)
