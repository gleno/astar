package astar

import (
	"gopkg.in/ini.v1"
	"log"
)

// Config allows to send messages to a webhook
type Config struct {
	*ini.File
	configPath string
}

// NewConfig creates a Config based on configFile
func NewConfig(configFile string) *Config {
	config, err := ini.Load(configFile)
	if err != nil {
		config = ini.Empty()
	}
	return &Config{config, configFile}
}

// Save saves the Config
func (c *Config) Save() error {
	err := c.SaveTo(c.configPath)
	if err != nil {
		log.Println("Cannot save configuration file to " + c.configPath)
	}
	return err
}
